'use strict';

const GAMES = {
  apex: {
	  name: 'Apex Legends',
    values: [
      'Comments on Loba\'s ass!',
	    'Forgets to use Tactical/Ult!',
	    'Choke!',
      'Clutch!',
      'Wins game',
      'Third partied!',
	    'Gets top damage',
      'Gets top kills',
	    '"REP-LA-KATE-OR"',
      'Asks for previoulsy pinged item',
	    '"Brippletake"\n(Triple Take)',
			'Ok, one more.\n(But plays 3+ more because that one sucked.)',
			'Prezzy for Fuseyy',
			'"They\'re a one hit!"\n(But they weren\'t)',
			'Picked up the wrong gun!',
			'Doesn\'t realize she\'s jumpmaster!',
			'Falls to death from missed jump!'
    ]	
  },
  hunt: {
    name: 'Hunt Showdown',
	  values: [
		  'Most kills!',
			'Southern accent',
      'Choke!',
      'Clutch!',
      'Wins game',
	    'Died to AI!',
	    'Scared some crows!',
      'Gets most kills',
	    'Accidently fires gun!',
			'Bit by dog!',
			'Popped an Immolator!',
			'Attacked by bees!',
			'Scared Crows!',
			'Extracts with four bounties!'
    ]
  }
}

const generalTiles = [
  'Australian accent',
  'British accent',
  'Speaks Japanese',
	'Singing K-Pop!',
  'Calls Splish rude!',
  'Finishing dinner on stream start!',
  'Chairstream!',
  'Tells Rob to give dog pills',
  'Goes to give dog pills',
  'Eating some chips!',
  'Splish says Maddy stinks',
  'Insults Splish',
  'Splish threatens bodily harm!',
  'Asks Rob to get food',
	'Complains about work!',
	'Clipped being a gam3r!',
	'Yells at viewers!',
	'Stream starts late!'
];
